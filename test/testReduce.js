const problemReduce = require("../reduce.js"); //Importing reduce.js file to problemReduce
const items = require("../array.js"); //Importing array.js file to items

function callback(result, current) {
  return result + current; //to get the sum of all the elements of array
}

const resultValue = problemReduce(items, callback, 50);

console.log(resultValue);
