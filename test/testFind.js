const problemFind = require("../find.js"); //Importing find.js file to problemfind
const items = require("../array.js"); //Importing array.js file to items

function callback(current) {
  return current > 2; //to find first value greater than 2
}

const finalResult = problemFind(items, callback);

console.log(finalResult);
