const problemFlatten = require("../flatten.js"); //Importing flaten.js file to problemFlatten

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

const flattenArray = problemFlatten(nestedArray);

console.log(flattenArray);
