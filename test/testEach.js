const problemEach = require("../each.js"); //Importing each.js file to problemEach
const items = require("../array.js"); //Importing array.js file to items

function callback(element, id) {
  console.log(`Value is ${element} at index ${id}`); //accessing element and id
}

problemEach(items, callback);
