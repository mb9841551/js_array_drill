const problemMap = require("../map.js"); //Importing map.js file to problemMap
const items = require("../array.js"); //Importing array.js file to items

function transformElement(element) {
  return element * 5; //to return an array where every element is multiplied by 5 of items array
}

console.log(problemMap(items, transformElement));
