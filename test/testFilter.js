const problemFilter = require("../filter.js"); //Importing filter.js file to problemFilter
const items = require("../array.js"); //Importing array.js file to items

function callback(value) {
  return value > 2; //to get array wheere all the elements are greater than 2
}

const finalArray = problemFilter(items, callback); //call the problemFiletr with callback as its argument

console.log(finalArray);
