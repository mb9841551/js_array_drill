// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array,
// in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

function map(elements, cb) {
  if (!Array.isArray(elements)) {
    //to check if elements is a valid array or not
    console.log("Invalid");
    return;
  }
  let newArray = [];
  for (let index = 0; index < elements.length; index++) {
    newArray.push(cb(elements[index])); //to push the value we got after callback function into newArray
  }
  return newArray;
}

module.exports = map; //to export the map function to its test file
