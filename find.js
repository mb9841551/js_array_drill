// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.

function find(elements, cb) {
  if (!Array.isArray(elements)) {
    //to check if elements is a valid array or not
    console.log("Invalid");
    return;
  }

  for (let index = 0; index < elements.length; index++) {
    let foundElement = cb(elements[index]); //Callback function
    if (foundElement) {
      return elements[index]; //if foundElement returns true , it returns the value of elements array which satisfies the condition.
    }
  }
}

module.exports = find;
