// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument

function each(elements, cb) {
  if (!Array.isArray(elements)) {
    //to check if elements is a valid array or not
    console.log("Invalid");
    return;
  }
  for (let index = 0; index < elements.length; index++) {
    cb(elements[index], index); //Callback function
  }
}

module.exports = each; //to export the each function
