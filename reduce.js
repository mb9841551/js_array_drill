// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

function reduce(elements, cb, startingValue) {
  if (!Array.isArray(elements)) {
    //to check if elements is a valid array or not
    console.log("Invalid");
    return;
  }
  if (startingValue === undefined) {
    startingValue = elements[0]; //startingValue is assigned as elements[0]

    for (let index = 1; index < elements.length; index++) {
      //we start the index from 1 as starting value is assigned as elements[0]
      startingValue = cb(startingValue, elements[index]); //Callback function
    }
  } else {
    for (let index = 0; index < elements.length; index++) {
      //we start the index from 0 as starting value was already given
      startingValue = cb(startingValue, elements[index]); //Callback function
    }
  }
  return startingValue;
}

module.exports = reduce; //to export the reduce function to its test file
