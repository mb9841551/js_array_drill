// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

function flatten(elements) {
  if (!Array.isArray(elements)) {
    //to check if elements is a valid array or not
    console.log("Invalid");
    return;
  }

  let finalArray = [];
  function recursionToFlattenElements(array) {
    for (let index = 0; index < array.length; index++) {
      if (Array.isArray(array[index])) {
        //to check if index of the array is array or not
        recursionToFlattenElements(array[index]); //if index of the array is an array , it goes again to recursive function
      } else {
        finalArray.push(array[index]); //if the index of array is not an array , we push it into finalArray
      }
    }
    return finalArray;
  }
  return recursionToFlattenElements(elements); //Calling and finally return the output from recursive functions
}

module.exports = flatten; //to export the flatten function to its test file
