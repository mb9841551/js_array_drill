// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test

function filter(elements, cb) {
  if (!Array.isArray(elements)) {
    //to check if elements is a valid array or not
    console.log("Invalid");
    return;
  }
  let finalArray = [];
  for (let index = 0; index < elements.length; index++) {
    let condition = cb(elements[index]); //Callback function
    if (condition) {
      finalArray.push(elements[index]); //if callback returns true , push the element into finalArray else it is empty array.
    }
  }
  return finalArray;
}

module.exports = filter; //to export the filter function to its test file.
